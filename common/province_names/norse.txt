1 = "Svíthjódh" #Agnafit
2 = "Austrgautland" #Linköking = Lyngkaupstadhur
3 = "Moere" #Kalmar = Kalmarnir
4 = "Bergslagen" #Vasteras = Vaestraaros
5 = "Vaermaland" #Tingvalla
6 = "Lundurland" #Lund = Lundur
7 = "Skaraland"
8 = "Jaernbaeraland"
9 = "Helsingjaland" #Gävle = Gaevleaegarna
10 = "Jamptaland" #Frösön = Fraeseyjar
11 = "Vestrbotn" #Umea = Úmaár
12 = "Sjáland" #Kobenhavn = Kaupmannahöfn
13 = "Slésvik" #Hedeby = Heidhabyr
14 = "Fjón" #Odense = Ódhinsvé
15 = "Vestrjotland" #Ribe = Rípar
16 = "Baahuslen"
17 = "Vestrfold" #Oslo = Kauphágur
18 = "Laepmork"
19 = "Austrbotn" #Korsholm = Korsholmr
20 = "Trondalog" #Trondheim = Nidharós 
21 = "Hálogaland"
22 = "Eystridalr" #Hamar = Hamarr
23 = "Hordhaland" #Bergen = Björgyn 
24 = "Ryggjafylki" #Stavanger = Stafangr
25 = "Gutland" #Visby = Vísbýr
26 = "Halland" #Halmstad = Halmstaede
27 = "Árbaeland" #Abo = Árbae
28 = "Nýrland" #Borga = Borgár
29 = "Tafaeistaland" #Vanaja
30 = "Vébjorgrland" #Vyborg = Vébjorgr
31 = "Savolask" #Olofsborg = Ólafrsborgr
32 = "Keskholmrland" #Kexholm = Keskholmr
33 = "Nyenland" #Nöteborg = Noeteborgr
34 = "Ingrmanland" #Koporye = Koporje
35 = "Eysysla" #Arensburg = Oernborgr
36 = "Eistland" #Reval = Lindanaes
37 = "Lífland"
38 = "Rigaborgr" #Riga = Rigaborgr
39 = "Kurland" #Grobina = Grobin
39 = "Memelland" #Memel = Memelborgr
41 = "Samland" #Kaup
42 = "Ermland" #Allenstein = Alnasteinn
43 = "Kasiuben" #Gdansk = Dansvik
44 = "Hamborgr" #Hamburg = Hamborgr
45 = "Libika" #Lübeck = Libika
46 = "Raudhstokkurland" #Rostock = Raudhstokkur
47 = "Straela" #Stralsund = Straela
48 = "Jómsborgr"
53 = "Lyneborgr" #Lüneburg = Lyneborgr
54 = "Stodhuborgr" #Stade = Stodhuborgr
55 = "Aldinborgr" #Oldenburg = Aldinborgr
57 = "Brunsvyk" #Brunswick = Brunsvyk
70 = "Stódhgardhur" #Sttutgart = Stódhgardhur
73 = "Isindal" #Innsbruck = Isinbriggiu
75 = "Stransborgr" #Strassburg = Stransborgr
78 = "Meginzuborgr" #Mainz = Meginzuborgr
79 = "Virzinborgr" #Wurzburg = Virzinborgr
85 = "Kolnisborgr" #Köln = Kolnisborgr
90 = "Brygge" #Bruges = Brygge
92 = "Bryssel" #Brussels = Bryssel
96 = "Sudhrsjáland" #Middelburg = Medhalborgr
97 = "Erilstífla" #Amsterdam = Erilstífla
98 = "Trekt" #Utrecht = Trekt
99 = "Geldern" #Gelre = Geldern
100 = "Frísland" #Leeuwarden = Lintarwrde
101 = "Genúa" #Genoa = Genúa
104 = "Melansborgr" #Milan = Melansborgr
108 = "Bernar" #Verona = Bernar
110 = "Trentarland" #Trent = Trentar
112 = "Feneyjar" #Venice = Feneyjar
113 = "Fera" #Ferrara = Fera
115 = "Pisis" #Pisa = Pisis
116 = "Florens" #Firenze = Florens
117 = "Langasyn" #Siena = Langasyn
118 = "Rómaborgr" #Roma = Rómaborgr
134 = "Vinarborgr"
142 = "Maríuhofn"
151 = "Miklagardhr"
163 = "Krít"
167 = "Rúdhuborgr" #Rouen = Rúdhuborgr
168 = "Kadhum" #Caen = Kadhum
172 = "Namsborgr"
180 = "Austrpeituland" #Poitiers = Peituborgr
183 = "Párísborgr"
186 = "Reimsborgr"
206 = "Jakubsland"
207 = "Gegio"
212 = "Gerun"
213 = "Barzulun"
219 = "Tulet"
227 = "Lizibón"
233 = "Kornbretaland"
234 = "Hamtúmscir" #Southampton = Sudhurhamtúm
235 = "Kentland" #Canterbury = Kantarabyrgt
236 = "Medhalsaeks" #London = Lundunaborgr
237 = "Öxnafurdhascir" #Oxford = Öxnafurdha
238 = "Austrsaeks" #Chelmsford = Kelmfurdha
239 = "Glodhsystraborgrscir" #Gloucester = Glodhsystraborgr
240 = "Scrobbesbyrgtscir" #Shrewsbury = Scrobbesbyrgt
241 = "Glaemorgaen" #Swansea = Sweynsey
242 = "Vinedh" #Caernarvon = Kaerenarborgr
243 = "Lindsborgrscir" #Lincoln = Lindsborgr
244 = "Lonborgrscir" #Lancaster = Lonborgr
245 = "Jórvíkscir" #York = Jórvík
246 = "Nordhymbraland" #Newcastle = Nýrkastali
247 = "Kumbraland" #Carlisle = Luelborgr
248 = "Lodhen" #Edinburgh = Edinborgr
249 = "Kluddal" #Wigtown = Hwithus
250 = "Perdhland" #Perth = Perdh
251 = "Apardjónland" #Aberdeen = Apardjón
252 = "Nisfljótland" #Inverness = Nisfljót
253 = "Vestrsudhreyjar" #Stornoway = Stjórnavágr
275 = "Palteskja"
280 = "Kaenugardhr"
286 = "Asof" #Azov = Asofborgr
293 = "Miliniska"
310 = "Hólmgardhr"
315 = "Finnmork" #City could be Geirsver
320 = "Roda"
321 = "Kípr"
333 = "Staerrey"
341 = "Tunsborgr"
354 = "Tripulis"
358 = "Alexandersborgr"
369 = "Orkneyjar" #Kirkwall = Kirkjuvágr 
370 = "Vestisland" #Reykjavik = Reykjarvik 
371 = "Austisland" #Akureyri
372 = "Tirovenstadhr" #Dungannon = Geanannsborgr 
373 = "Dyflinstadhr" #Dublin = Dyflin 
374 = "Leinstadhr" #Wexford = Weisufjordh 
375 = "Tuadhmunstadhr" #Limerick = Hlymrekr 
376 = "Sudhrkonnaktastadhr" #Galway = Galvágr
379 = "Jórsalir" 
981 = "Leifsbudhir"
1177 = "Höfdhaland" #Cape Town = Höfdhaborg
1283 = "Vestrsudhreyjar" #Stornoway = Stjórnavágr
1749 = "Karlsá"
1754 = "Móramar"
1758 = "Hanabruinborgr"
1761 = "Vernisa"
1775 = "Holtsetaland" #Kiel = Kíl
1776 = "Gandhvikland" #Gandhvikborgr
1777 = "Nordhrkirilialand" #Kem = Kemborgr
1841 = "Trusoland" #Truso
1842 = "Virland"
1854 = "Akrsborgr"
1855 = "Saett"
1860 = "Nordhfolk" #Norwich = Nordhvik
1861 = "Djúra'bý"
1868 = "Austrborgr"
1874 = "Brimar"
1876 = "Frakkafurdha"
1930 = "Árland" #Sund
1933 = "Bár"
1956 = "Súrsdalr"
1961 = "Aldeigja" #Staraya Ladoga = Aldeigjuborg
1978 = "Hjaltland" #Lerwick = Leirvík
1979 = "Faereyjar" #Tórshavn = Tórshöfn
1981 = "Burgundaholmr" #Ronne = Rotnaeby
1982 = "Bleking" #Ronneby = Rotnaeby
1983 = "Láland" #Falster = Falstur
1984 = "Nordjotland" #Aalborg = Álaborg
1985 = "Naerríki" #Örebro = Eyrarsundborgr
2298 = "Átalsborgr"
2313 = "Antekya"
2752 = "Raumdsdael"
2753 = "Nerbón"
2972 = "Poddubrunnar"
2980 = "Lúka"
2988 = "Tarragun"
4110 = "Sudhrland"
4113 = "Kvenland" #Uleaborg = Uleárborgr
4114 = "Lule Laepmork" #Jokkmokk = Jokomuka
4118 = "Deasmunstadhr" #Cork = Corcach
4119 = "Nordhrkonnaktastadhr" #Sligo = Slicekborgr
4120 = "Kildhar" #Naas = Nasborgr
4121 = "Ulastadhr" #Carrickfergus = Fergussteinn
4123 = "Birkaland" #Birkala
4124 = "Sudhrkirilialand"
4130 = "Sumortýnsaetascir" #Bath = Laugborgr
4141 = "Thetmerski" #Meldorf = Moelthorp
4142 = "Austrjotland" #Aarhus = Árósar
4143 = "Vingulmork" #Moss = Mors 
4144 = "Thilamork" #Skien = Skidha 
4145 = "Agdir" #Mandal = Marnardalr 
4147 = "Rádhstofa"
4149 = "Kaldyng"
4174 = "Duracur"
4151 = "Finnbygdir"
4152 = "Kajanaland" #Kajana = Kajaneborgr 
4163 = "Aelvoeseland" #Elfsborg = Aelvoeseborgr 
4164 = "Dalsland" #Dalaborg = Dalaborgr
4165 = "Goenge" #Gladsax = Gladsask  
4166 = "Tiohaerad" #Vaxjo = Vaeskjoe  
4361 = "Roskborgrcir" #Berwick = Baravik
4362 = "Sudhrgaidheilstadr" #Dumfries = Frisborgr
4363 = "Nodhrgaidheilstadr" #Loch Awe = Lokobaborgr
4364 = "Austrsudhreyjar" #Skye = Skidh
4365 = "Mon" #Castletown = Rushenborgr
4366 = "Powystadr" #Montgomery = Gumarikfell
4367 = "Hymbraland" #Hull = Hulvik
4368 = "Skardhaborgrstadr" #Scarborough = Skardhaborgr
4369 = "Dehybardh" #Pembroke = Melrfjord
4370 = "Kambarbryggjascir" #Cambridge = Kambarbryggja
4371 = "Sudharsaeks" #Hastings = Haestingas
4372 = "Waeringvikscir" #Warwick = Waeringvik
4373 = "Defnascir" #Plymouth = Plymmudhr
4374 = "Dorsetescir" #Weymouth = Weymudhr
4375 = "Legaborgrscir" #Chester = Legaborgr
4376 = "Ledeborgr" #Leicester = Ledeborgr
4377 = "Urmunstadhr" #Kilkenny = Kilkainny  
4378 = "Iarmunstadhr" #Killarney = Kileýrn  
4379 = "Tethbastadhr" #Annaly = Anghaylborgr  
4380 = "Konalstadhr" #Donegal = Dýnangal  
4386 = "Vastrpeituland"
4389 = "Túskaland"
4552 = "Palenz"
4559 = "Manork"
4560 = "Iviza"
4728 = "Papey"
4742 = "Gozler"
4745 = "Rygen" #Bergen in Rügen = Björgyn  
4758 = "Boz"
4765 = "Mioluhús"
4771 = "Ferdhuborgr"
4785 = "Sjalfelt"