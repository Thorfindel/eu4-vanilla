branch=$(git rev-parse --abbrev-ref HEAD)
project=eu4
bucket_id=$project\-dev
build_key=jenkins-$project\-release_d-osx-$branch
currentdir=$(basename $(pwd))
destination=$(pwd)

curl http://libs-prod-01.paradox.private/dirty/devtools/pdsbuild/osx/pdsbuild -o pdsbuild
chmod +x pdsbuild

./pdsbuild deployscripts buildsync -s azure -p osx -c debug -b $project pull $build_key --destination $destination -i $bucket_id --latest True --zip True
